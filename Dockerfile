FROM centos:7

ENV LC_ALL=en_US.utf8

ENV DOCUMENT_ROOT_DIR=/var/www/html/public
ENV HTTPD_CONF_DIR=/etc/httpd/conf
ARG timezone
ENV timezone ${timezone:-"America/Sao_Paulo"}

RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
    http://rpms.remirepo.net/enterprise/remi-release-7.rpm \
    yum-utils \
    && yum-config-manager --enable remi-php73 \
    && yum install -y php php-mcrypt php-cli php-gd php-curl php-ldap php-zip php-fileinfo libXrender fontconfig libXext php-opcache php-pear php-mbstring httpd tzdata php-mssql php-redis php-memcached https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox-0.12.5-1.centos7.x86_64.rpm \
    && echo ${timezone} > /etc/timezone \
    && sed -i "s|;date.timezone =|date.timezone = ${timezone}|g" /etc/php.ini \
    && sed -i "s|ErrorLog \"logs/error_log\"|ErrorLog \"/dev/stderr\"|g" /etc/httpd/conf/httpd.conf \
    && sed -i "s|"${DOCUMENT_ROOT_DIR}"|${DOCUMENT_ROOT_DIR}/public|g" ${HTTPD_CONF_DIR}/httpd.conf \
    && sed -i "151s|None|All|" ${HTTPD_CONF_DIR}/httpd.conf \
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasil.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasil.crt \
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasilv2.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasilv2.crt \
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasilv4.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasilv4.crt \
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasilv5.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasilv5.crt \
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasilv6.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasilv6.crt \
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasilv7.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasilv7.crt \
    && update-ca-trust \
    && yum clean all \
    && rm -rf /var/cache/yum

WORKDIR /var/www/html

EXPOSE 80/tcp

CMD [ "/usr/sbin/apachectl","-DFOREGROUND" ]